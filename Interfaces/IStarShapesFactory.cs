using System;
using Android.Graphics;
using RatingWidget.Enumerations;

namespace RatingWidget.Interfaces
{
    internal interface IStarShapesFactory
    {
        IStarShape CreateShape(Point position, Int32 sizeInPx, StarState initialState);
    }
}