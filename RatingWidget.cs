using System;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Runtime;
using Android.Graphics;
using Android.Content;
using Android.Content.Res;
using Android.Views.Animations;
using RatingWidget.Interfaces;
using RatingWidget.Enumerations;

namespace RatingWidget
{
    [Register("ratingwidget.android.views.RatingWidget")]
    internal sealed class RatingWidget : View, View.IOnTouchListener, View.IOnHoverListener
    {
        private const String SavedInstanceStateRatingValueKey = "RatingKey";

        private const Int32 AnimationDurationInMilliseconds = 300;

        internal IStarShapesFactory ShapesFactory { get; set; }

        public Int32 Rating
        {
            get => _rating;
            set
            {
                _rating = (value >= 0) ? value : throw new ArgumentOutOfRangeException(nameof(value));
                UpdateAffectedShapesStates(_rating - 1, StarState.Marked);
                Invalidate();
            }
        }

        public RatingWidget(Context context, IAttributeSet attributes) : base(context, attributes)
        {
            SetOnTouchListener(this);
            SetOnHoverListener(this);

            TypedArray styledAttributes = context.ObtainStyledAttributes(
                attributes,
                Resource.Styleable.RatingWidget
            );

            _rating = styledAttributes.GetInteger(Resource.Styleable.RatingWidget_initialRating, 0);

            _starsNumber = styledAttributes.GetInteger(
                Resource.Styleable.RatingWidget_starsNumber,
                Resource.Integer.ratingWidgetDefaultStarsNumber
            );

            _shapes = new IStarShape[_starsNumber];

            _spaceBetweenStars = styledAttributes.GetDimensionPixelSize(
                Resource.Styleable.RatingWidget_spaceBetweenStars,
                Resources.GetDimensionPixelSize(Resource.Dimension.normalPadding)
            );

            Color fillColor = styledAttributes.GetColor(
                Resource.Styleable.RatingWidget_fillColor,
                Resource.Attribute.colorPrimaryDark
            );

            Color outlineColor = styledAttributes.GetColor(
                Resource.Styleable.RatingWidget_outlineColor,
                Resource.Attribute.colorPrimaryDark
            );

            Color highlightColor = styledAttributes.GetColor(
                Resource.Styleable.RatingWidget_highlightColor,
                Resource.Attribute.colorPrimary
            );

            ShapesFactory = new DefaultStarShapeFactory(fillColor, outlineColor, highlightColor);
        }

        protected override void OnRestoreInstanceState(IParcelable state)
        {
            if (state is Bundle bundle)
            {
                _rating = bundle.GetInt(SavedInstanceStateRatingValueKey);

                UpdateAffectedShapesStates(_rating - 1, StarState.Marked);
                Invalidate();
            }
        }

        protected override IParcelable OnSaveInstanceState()
        {
            Bundle state = new Bundle();
            state.PutInt(SavedInstanceStateRatingValueKey, _rating);
            return state;
        }

        protected override void OnMeasure(Int32 widthMeasureSpec, Int32 heightMeasureSpec)
        {
            Int32 height = MeasureSpec.GetSize(heightMeasureSpec);
            Int32 width = MeasureSpec.GetSize(widthMeasureSpec);

            if (width == 0 || height == 0)
            {
                return ;
            }

            _starSize = (width / _starsNumber) - _spaceBetweenStars + _spaceBetweenStars / _starsNumber;

            height = _starSize;
            SetMeasuredDimension(width, height);

            InstantiateShapes();
            UpdateAffectedShapesStates(_rating - 1, StarState.Marked);
        }

        public override void Draw(Canvas canvas)
        {
            base.Draw(canvas);

            for (Int32 i = 0; i < _starsNumber; i++)
            {
                _shapes[i].Render(canvas);
            }
        }

        public Boolean OnHover(View view, MotionEvent @event)
        {
            Point motionPosition = new Point((Int32)@event.GetX(), (Int32)@event.GetY());
            ProcessMotion(StarState.Highlited, motionPosition);
            return true;
        }

        public Boolean OnTouch(View view, MotionEvent @event)
        {
            Point motionPosition = new Point((Int32)@event.GetX(), (Int32)@event.GetY());
            ProcessMotion(StarState.Marked, motionPosition);
            return true;
        }

        private void ProcessMotion(StarState affectedShapesState, Point motionPosition)
        {
            Int32 index = FindAffectedShapeIndex(motionPosition);
            if (index < 0)
            {
                return ;
            }

            _rating = index + 1;

            UpdateAffectedShapesStates(index, affectedShapesState);
            AnimateAffectedShapes(index);
        }

        private Int32 FindAffectedShapeIndex(Point motionPosition)
        {
            for (Int32 i = 0; i < _shapes.Length; i++)
            {
                if (_shapes[i].CheckCollisionWith(motionPosition))
                {
                    return i;
                }
            }

            return -1;
        }

        private void UpdateAffectedShapesStates(Int32 affectedShapeIndex, StarState affectedShapesState)
        {
            // if (affectedShapeIndex == _shapes.Length - 1)
            // {
            //     for (Int32 i = 0; i < _shapes.Length; i++)
            //     {
            //         if (_shapes[i].State != StarState.Empty)
            //         {
            //             _shapes[i].State = StarState.Empty;
            //         }
            //     }
            //
            //     return ;
            // }

            for (Int32 i = 0; i < _shapes.Length; i++)
            {
                _shapes[i].State = (i <= affectedShapeIndex) ? affectedShapesState : StarState.Empty;
            }
        }

        private void InstantiateShapes()
        {
            if (ShapesFactory == null)
            {
                throw new InvalidOperationException("Shapes factory is null.");
            }

            Point shapeCenter = new Point(_starSize / 2, _starSize / 2);
            Int32 xStep = _starSize + _spaceBetweenStars;

            for (Int32 i = 0; i < _shapes.Length; i++)
            {
                _shapes[i] = ShapesFactory.CreateShape(new Point(shapeCenter), _starSize, StarState.Empty);
                _shapes[i].InvalidationRequiredCallback += (r) => Invalidate();
                shapeCenter.X += xStep;
            }
        }

        private void AnimateAffectedShapes(Int32 affectedShapeIndex)
        {
            for (Int32 i = 0; i <= affectedShapeIndex; i++)
            {
                _shapes[i].Animate(new AccelerateDecelerateInterpolator(), AnimationDurationInMilliseconds);
            }
        }

        private Int32 _rating;
        private Int32 _starSize;

        private readonly IStarShape[] _shapes;

        private readonly Int32 _starsNumber;
        private readonly Int32 _spaceBetweenStars;
    }
}