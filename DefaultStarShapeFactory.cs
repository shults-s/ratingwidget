using System;
using Android.Graphics;
using RatingWidget.Interfaces;
using RatingWidget.Enumerations;

namespace RatingWidget
{
    internal sealed class DefaultStarShapeFactory : IStarShapesFactory
    {
        public DefaultStarShapeFactory(Color fillColor, Color outlineColor, Color highlightColor)
        {
            _fillColor = fillColor;
            _outlineColor = outlineColor;
            _highlightColor = highlightColor;
        }

        public IStarShape CreateShape(Point position, Int32 sizeInPx, StarState initialState)
        {
            return new HeartStarShape(position, sizeInPx, _fillColor, _outlineColor, _highlightColor);
        }

        private readonly Color _fillColor;
        private readonly Color _outlineColor;
        private readonly Color _highlightColor;
    }
}