using System;
using Android.Graphics;
using Android.Animation;
using RatingWidget.Enumerations;

namespace RatingWidget.Interfaces
{
    internal interface IStarShape
    {
        event Action<Rect> InvalidationRequiredCallback;

        StarState State { get; set; }

        void Render(Canvas canvas);

        Boolean CheckCollisionWith(Point point);

        void Animate(ITimeInterpolator interpolator, Int32 durationInMilliseconds);
    }
}