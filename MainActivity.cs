using Android.OS;
using Android.Runtime;
using AndroidX.AppCompat.App;

namespace RatingWidget
{
    [Register("ratingwidget.android.views.MainActivity")]
    public sealed class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            System.Single xDpi = Resources.DisplayMetrics.Density;
        }
    }
}