using System;
using Android.Graphics;
using Android.Animation;
using RatingWidget.Interfaces;
using RatingWidget.Enumerations;

namespace RatingWidget
{
    internal sealed class HeartStarShape : Java.Lang.Object, IStarShape, ValueAnimator.IAnimatorUpdateListener
    {
        public event Action<Rect> InvalidationRequiredCallback;

        public StarState State { get; set; }

        public HeartStarShape(Point centerPosition, Int32 sizeInPx, Color fillColor, Color outlineColor,
            Color highlightColor)
        {
            _paint = new Paint(PaintFlags.AntiAlias)
            {
                StrokeWidth = Math.Max(sizeInPx / 23, 1)
            };

            _path = new Path();

            _fillColor = fillColor;
            _outlineColor = outlineColor;
            _highlightColor = highlightColor;

            _sizeInPx = (sizeInPx > 0)
                ? sizeInPx
                : throw new ArgumentOutOfRangeException(nameof(sizeInPx), "Number must be positive.");

            Int32 valueShift = _sizeInPx / 15;
            _animator = ValueAnimator.OfInt(
                _sizeInPx,
                _sizeInPx + valueShift,
                _sizeInPx
            );

            _animator.AddUpdateListener(this);

            // Format: xStart, yStart, xControl1, yControl1, xControl2, yControl2, xEnd, yEnd
            _coefficients = new Single[]
            {
                -0.5F, 0.205F,  -0.4F,   0.674F, 0.007F, 0.498F,   0F,  0.214F, // Left  top (half ass).
                   0F, 0.214F, 0.095F,     0.7F, 0.536F, 0.474F, 0.5F,  0.205F, // Right top (half ass).
                 0.5F, 0.205F,   0.5F,  -0.087F, 0.076F, -0.17F,   0F,   -0.5F, // Right bottom.
                   0F,  -0.5F, -0.035F, -0.226F, -0.55F, -0.22F, -0.5F, 0.205F  // Left  bottom.
            };

            _points = new Single[_coefficients.Length];

            _centerPosition = centerPosition ?? throw new ArgumentNullException(nameof(centerPosition));

            ComputePointsWithRespectShapeSize();
        }

        public void Render(Canvas canvas)
        {
            // _paint.Color = Color.Navy;
            // _paint.SetStyle(Paint.Style.Fill);
            // canvas.DrawCircle(_centerPosition.X, _centerPosition.Y - _sizeInPx / 15, _sizeInPx / 2, _paint);

            switch (State)
            {
                case StarState.Highlited:
                    _paint.SetStyle(Paint.Style.Fill);
                    _paint.Color = _highlightColor;
                    break;

                case StarState.Marked:
                    _paint.SetStyle(Paint.Style.Fill);
                    _paint.Color = _fillColor;
                    break;

                case StarState.Empty:
                    _paint.SetStyle(Paint.Style.Stroke);
                    _paint.Color = _outlineColor;
                    break;

                default:
                    throw new NotSupportedException();
            }

            _path.Reset();
            _path.MoveTo(_points[0], _points[1]);
            _path.CubicTo(_points[2], _points[3], _points[4], _points[5], _points[6], _points[7]);
            _path.CubicTo(_points[10], _points[11], _points[12], _points[13], _points[14], _points[15]);
            _path.CubicTo(_points[18], _points[19], _points[20], _points[21], _points[22], _points[23]);
            _path.CubicTo(_points[26], _points[27], _points[28], _points[29], _points[30], _points[31]);
            _path.Close();

            canvas.DrawPath(_path, _paint);
        }

        public void OnAnimationUpdate(ValueAnimator animation)
        {
            _sizeInPx = (Int32)animation.AnimatedValue;
            ComputePointsWithRespectShapeSize();

            Int32 halfSize = _sizeInPx / 2;

            Rect updatedRect = new Rect(
                _centerPosition.X - halfSize,
                _centerPosition.Y - halfSize,
                _centerPosition.X + halfSize,
                _centerPosition.Y + halfSize
            );

            InvalidationRequiredCallback?.Invoke(updatedRect);
        }

        public void Animate(ITimeInterpolator interpolator, Int32 durationInMilliseconds)
        {
            _animator.End();
            _animator.SetDuration(durationInMilliseconds);
            _animator.SetInterpolator(interpolator);
            _animator.Start();
        }

        public Boolean CheckCollisionWith(Point point)
        {
            Int32 centerX = _centerPosition.X;
            Int32 centerY = _centerPosition.Y - _sizeInPx / 15;
            Int32 pX = point.X;
            Int32 pY = point.Y;
            Int32 radius = _sizeInPx / 2;
            return ((pX - centerX) * (pX - centerX) + (pY - centerY) * (pY - centerY)) < (radius * radius);
        }

        private void ComputePointsWithRespectShapeSize()
        {
            Single scaleFactor = (935F * _sizeInPx) / 992F;
            for (Int32 i = 0; i < _points.Length; i++)
            {
                _points[i] = _coefficients[i] * scaleFactor;
                _points[i] = (i % 2 == 0) ? _centerPosition.X + _points[i] : _centerPosition.Y - _points[i];
            }
        }

        private readonly Color _fillColor;
        private readonly Color _outlineColor;
        private readonly Color _highlightColor;

        private Int32 _sizeInPx;
        private readonly Point _centerPosition;

        private readonly Single[] _points;
        private readonly Single[] _coefficients;

        private readonly Path _path;
        private readonly Paint _paint;
        private readonly ValueAnimator _animator;
    }
}